import React, { Component } from 'react'
import Question from './question/Question'

export default class Exercise extends Component {

    state = {
        activeQuestion: this.props.exercise.questions[0]
    }
 
    render() {

        let { exercise, toggleModal } = this.props

        return (
            <div className="exercise-wrapper">
                <h1>{ exercise.subject } - { exercise.level }</h1>
                { exercise.questions.map((question, index) => <Question toggleModal={toggleModal} question={question} number={index + 1} key={question.id} />) }
            </div>
        )
    }

}