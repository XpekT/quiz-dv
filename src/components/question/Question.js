import React, { Component } from 'react'
import './question.css'

export default class Question extends Component {

    state = {
        option: '',
        formula: '',
        isCorrect: false,
        hasAnswered: false,
        attempts: 0,
        errors: [],
        showVideos: false
    }

    handleOptionChanged = (e) => {
        this.setState({ option: e.target.value })
    }

    handleFormulaChanged = (e) => {
        this.setState({ formula: e.target.value })
    }
    
    retry = () => {
        this.setState({ option: '', formula: '', hasAnswered: false, isCorrect: false, attempts: 0, errors: [] })
    }

    check = (question) => {

        let userChoiceValue = Number(this.state.option)
        let answer = question.answer.value
        let userChoiceFormula = String(this.state.formula).toLowerCase()

        if(!userChoiceValue || !userChoiceFormula) {

            alert("É necessário escolher uma alínea e escrever a fórmula!")

        } else {
         
            if(userChoiceValue === answer && (userChoiceFormula === String(question.answer.formulaPT).toLowerCase() || userChoiceFormula === String(question.answer.formulaEN).toLowerCase())) {
         
                this.setState({ isCorrect: true, attempts: 0, hasAnswered: true })
         
            } else {

                this.setState({ errors: [], hasAnswered: true })

                if(userChoiceValue !== answer) {
                    
                    console.log("A resposta correta é: " + answer + "!")

                    this.setState((state, props) => ({
                        errors: [...state.errors, 'value']
                    }))
                
                }
                
                if(userChoiceFormula !== question.answer.formulaPT && userChoiceFormula !== question.answer.formulaEN) {
                    
                    console.log("A fórmula correta é: " + question.answer.formulaPT + " ou " + question.answer.formulaEN + ".")
                    
                    this.setState((state, props) => ({
                        errors: [...state.errors, 'formula']
                    }))
                
                }

                if(this.state.attempts >= 3) {
                    
                    this.setState({ showVideos: true })
                    //this.retry()
                
                } else {
                
                    this.setState((state, props) => ({
                        isCorrect: false,
                        attempts: state.attempts + 1
                    }))
                
                }

            }
        }

    }

    indexTranslator = (number) => {

        switch(number) {
            case 0:
                return 'A'
            case 1:
                return 'B'
            case 2:
                return 'C'
            case 3:
                return 'D'
            default:
                return number
        }

    }

    render() {

        let { question, number, toggleModal } = this.props

        return (
            <div className="question-wrapper">
                <div className="file-wrapper">
                    <a href={question.file}>
                        <i className="fas fa-file-download file-icon"></i>
                    </a>
                </div>
                <div className="question-header">
                    <h2>{number} - { question.question }</h2>
                </div>
                <div className="question-image-wrapper">
                    <img src={ question.image } alt="question" />
                </div>
                <div className="form-wrapper">
                    <div className="options-wrapper">
                        { question.options.map((option, index) => {
                            return (
                                <div className="label-wrapper" key={ index }>
                                    <label htmlFor={ `answer-${index}` }>
                                        <span className="index">
                                            { this.indexTranslator(index) }.
                                        </span>
                                        <span className="option">{ option }</span>
                                        <input 
                                            disabled={ this.state.option !== option && this.state.option !== '' && this.state.isCorrect } 
                                            onChange={ this.handleOptionChanged } 
                                            id={ `answer-${index}` } 
                                            type="checkbox" 
                                            name={ `answer-${index}` } value={ option }
                                        />
                                    </label>
                                </div>
                            )
                        }) }
                    </div>
                    { question.answer.formulaRequired &&
                        <div className="formula-wrapper">
                            <label htmlFor="formula">Fórmula</label>
                            <input 
                                id="formula" 
                                type="text" 
                                name="formula" 
                                placeholder="Indique a fórmula usada para efetuar o cálculo"
                                autoComplete="off"
                                value={ this.state.formula }
                                disabled={ (this.state.hasAnswered && this.state.isCorrect) ? 'disabled' : '' }
                                onChange={ this.handleFormulaChanged }
                            />
                        </div>
                    }
                    <div className="buttons-wrapper">
                        <button 
                            type="button" 
                            onClick={ () => this.check(question) }
                            disabled={ (this.state.hasAnswered && this.state.isCorrect) ? 'disabled' : '' }
                        >
                            <i className="far fa-save"></i>
                            Confirmar!
                        </button>
                        { (this.state.option !== '' && this.state.options !== '' && this.state.hasAnswered) &&
                            <div className="answered">
                                { this.state.isCorrect && 
                                    <i className="correct far fa-check-circle"></i>
                                }
                                { !this.state.isCorrect &&
                                    <i className="incorrect far fa-times-circle"></i>
                                }
                            </div>
                        }
                    </div>
                    { this.state.showVideos &&
                        <div className="video-thumbnail-wrapper">
                            { question.videos.map(video => {
                                return (
                                    <div className="video" key={ video.id } onClick={ () => toggleModal(video.src) }>
                                        <img src={ video.thumbnail } alt="thumbnail"/>
                                        <small><i className="fas fa-video"></i> { video.title }</small>
                                    </div>
                                )
                            }) }
                        </div>
                    }
                </div>
            </div>
        )
    }

}