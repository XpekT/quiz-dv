import React, { Fragment } from 'react'
import './modal.css'

export default (props) => {

    let { show, close, video } = props

    if(show) {

        return (
            <div className="modal-wrapper">
                <div className="modal">
                    <div className="modal-header">
                        <h1>Veja este vídeo!</h1>
                    </div>
                    <i className="close-modal far fa-times-circle" onClick={close}></i>
                    <div className="video-wrapper">
                        <iframe src={video} frameBorder="0" title="video" allowFullScreen></iframe>
                    </div>
                </div>
            </div>
        )
    
    } else {
        return <Fragment />
    }

}