import React, { Component } from 'react';
import './App.css';
import _ from 'underscore'
import Exercise from './components/Exercise'
import Modal from './components/modal/Modal'

class App extends Component {

  state = {
    toggleModal: false,
    video: null
  }

  handleToggleModal = (video) =>  {
    
    this.setState((state, props) => ({
      toggleModal: !state.toggleModal,
      video
    }))
  
  }

  closeModal = () => {
    this.setState({ toggleModal: false })
  }

  exercises = [
    { 
      subject: 'Excel',
      level: 0,
      id: Date.now() * _.random(1, 100),
      questions: [
        { 
          id: Date.now() * _.random(1, 100),
          question: 'Qual é o valor da soma das células A2 e B2?', 
          answer: { value: 20, formulaRequired: true, formulaEN: '=SUM(A2;B2)', formulaPT: '=SOMA(A2;B2)' },
          videos: [ { id: Date.now() * _.random(1, 100), title: 'Excel - Função SOMA/SUM', src: 'https://www.youtube.com/embed/PfVSX7CNLKg', thumbnail: 'https://i.ytimg.com/vi/PfVSX7CNLKg/hqdefault.jpg' } ],
          image: '/images/sum_avg.png',
          file: 'https://firebasestorage.googleapis.com/v0/b/vt-dev-12345.appspot.com/o/Soma%20e%20M%C3%A9dia.xlsx?alt=media&token=b260643e-a56d-46f2-a7a2-a9e00fc2b44c',
          options: [10, 5, 20, 15]
        },
        {
          id: Date.now() * _.random(1, 100),
          question: 'Qual é a média dos valores das células A2 e B2?', 
          answer: { value: 10, formulaRequired: true, formulaEN: '=AVERAGE(A2;B2)', formulaPT: '=MÉDIA(A2;B2)' },
          videos: [ { id: Date.now() * _.random(1, 100), title: 'Excel - Função MÉDIA/AVERAGE', src: 'https://www.youtube.com/embed/5_OHS-18RbU', thumbnail: 'https://i.ytimg.com/vi/5_OHS-18RbU/hqdefault.jpg' } ],
          image: '/images/sum_avg.png',
          file: 'https://firebasestorage.googleapis.com/v0/b/vt-dev-12345.appspot.com/o/Soma%20e%20M%C3%A9dia.xlsx?alt=media&token=b260643e-a56d-46f2-a7a2-a9e00fc2b44c',
          options: [10, 5, 20, 15]
        },
        {
          id: Date.now() * _.random(1, 100),
          question: 'Qual é o valor minímo coluna A?',
          answer: { value: -1, formulaRequired: true, formulaEN: '=MIN(A1:A21)', formulaPT: '=MÍNIMO(A1:A10)' },
          videos: [ { id: Date.now() * _.random(1, 100), title: 'Excel - Função MÍNIMO/MIN', src: 'https://www.youtube.com/embed/jjFj92LdJAQ', thumbnail: 'https://i.ytimg.com/vi/jjFj92LdJAQ/hqdefault.jpg' } ],
          image: '/images/min_max.png',
          file: 'https://firebasestorage.googleapis.com/v0/b/vt-dev-12345.appspot.com/o/M%C3%ADnimo%20-%20M%C3%A1ximo.xlsx?alt=media&token=9a0f5c1b-88ae-46be-8ef9-86aa16287041',
          options: [1, 5, -1, 3]
        },
        {
          id: Date.now() * _.random(1, 100),
          question: 'Qual é o valor máximo coluna A?',
          answer: { value: 312, formulaRequired: true, formulaEN: '=MAX(A1:A21)', formulaPT: '=MÁXIMO(A1:A10)' },
          videos: [ { id: Date.now() * _.random(1, 100), title: 'Excel - Função MÁXIMO/MAX', src: 'https://www.youtube.com/embed/0mhr9I4M1-U', thumbnail: 'https://i.ytimg.com/vi/0mhr9I4M1-U/hqdefault.jpg' } ],
          image: '/images/min_max.png',
          file: 'https://firebasestorage.googleapis.com/v0/b/vt-dev-12345.appspot.com/o/M%C3%ADnimo%20-%20M%C3%A1ximo.xlsx?alt=media&token=9a0f5c1b-88ae-46be-8ef9-86aa16287041',
          options: [41, 23, 21, 312]
        }
      ]
    }
    
  ]

  render() {
    return (
      <div className="App">
        { this.exercises.map(exercise => {
          return <Exercise toggleModal={this.handleToggleModal} exercise={exercise} key={exercise.id} />
        })}
        <Modal close={this.closeModal} show={this.state.toggleModal} video={this.state.video} />
      </div>
    );
  }
}

export default App;
